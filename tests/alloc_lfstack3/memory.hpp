#pragma once

#include <twist/stdlike/atomic.hpp>

#include <vector>
#include <string>
#include <algorithm>

using twist::checker::StateDescription;

struct Node {
  twist::stdlike::atomic<Node*> next_{nullptr};
  bool free_{true};
  std::string label_{};

  bool IsReleased() const {
    return free_;
  }

  void Release() {
    // slower
    // MC_CHECK(!free_, "Already released");

    /*#if defined(TWIST_TRACE)
        std::cout << "released " << std::hex << (uint64_t)this << std::dec
                  << std::endl;
    #endif*/

    free_ = true;
  }

  void Acquire() {
    // slower
    // MC_CHECK(free_, "Already acquired");

    /*#if defined(TWIST_TRACE)
        std::cout << "released " << std::hex << (uint64_t)this << std::dec
                  << std::endl;
    #endif*/

    free_ = false;
  }

  void SetLabel(const std::string& name) {
    std::ostringstream os;
    os << name << " (" << std::hex << this << std::dec << ") ";
    label_ = os.str();
  }

  std::string GetLabel() const {
    return label_;
  }
};

class MemoryPool {
 public:
  MemoryPool(size_t count) {
    nodes_ = std::vector<Node>{count};
    char name = 'A';

    for (auto it = nodes_.rbegin(); it != nodes_.rend(); ++it) {
      it->SetLabel(std::string(1, name++));
    }
  }

  std::vector<Node>& GetNodes() {
    return nodes_;
  }

  size_t ReleasedCount() const {
    size_t cnt = 0;
    for (auto& node : nodes_) {
      if (node.IsReleased()) {
        ++cnt;
      }
    }
    return cnt;
  }

  Node* Get(size_t index) {
    return &nodes_[index];
  }

  Node* Acquire(size_t index) {
    for (auto& node : nodes_) {
      if (node.IsReleased()) {
        if (index == 0) {
          node.Acquire();
          return &node;
        }
        --index;
      }
    }
    TWIST_UNREACHABLE();
  }

  StateDescription DescribeState(const std::string& name = "Memory") {
    StateDescription description{name};
    for (auto& node : nodes_) {
      description.Add(node.GetLabel(), !node.free_);
    }
    return description;
  }

 private:
  std::vector<Node> nodes_;
};
