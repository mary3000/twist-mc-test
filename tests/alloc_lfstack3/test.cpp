#include <twist/checker/core/api.hpp>

#include <twist/test_framework/test_framework.hpp>

#include <twist/support/random.hpp>

#include <twist/stdlike/thread.hpp>

#include <iostream>
#include <cmath>
#include <twist/checker/test.hpp>

#include "alloc_lfstack.hpp"
#include "memory.hpp"

using namespace twist::checker;

TEST_SUITE(AllocLockFreeStack) {
  SIMPLE_CHECKER_TEST(Spec) {
    MemoryPool pool{3};
    LFAllocator stack{pool};

    Trace("/tmp/alloc_lfstack/v3.log");

    PrintState([&stack, &pool]() {
      StateDescription d{"LFAlloc"};
      d.Add(stack.DescribeState());
      d.Add(pool.DescribeState());
      return d;
    });

    AddInvariant([&stack]() -> std::pair<bool, std::string> {
      ForkGuard guard;

      auto check_list = [](Node* node) {
        int i = 0;
        while (node != nullptr) {
          if (node->IsReleased()) {
            return false;
          }
          node = node->next_.load();
          ++i;
        }
        return true;
      };

      if (!check_list(stack.Top())) {
        return {false, "ItemList: assertion failed"};
      }

      if (!check_list(stack.TopPending())) {
        return {false, "PendingList: assertion failed"};
      }

      return {true, ""};
    }).ExpectFail();

    auto worker = [&stack, &pool]() {
      while (true) {
        size_t available = pool.ReleasedCount();
        int node_index = twist::RandomUInteger(available) - 1;
        if (node_index == -1) {
          SHOW_NOTE("Alloc(): before");
          stack.Alloc();
          SHOW_NOTE("Alloc(): after");
        } else {
          SHOW_NOTE("Free(): before");
          Node* to_free = pool.Acquire(node_index);
          stack.Free(to_free);
          SHOW_NOTE("Free(): after");
        }
      }
    };

    std::vector<twist::stdlike::thread> threads;
    for (size_t i = 0; i < 3; ++i) {
      threads.emplace_back(worker);
    }

    for (auto& th : threads) {
      th.join();
    }
  }
}

RUN_ALL_TESTS()
