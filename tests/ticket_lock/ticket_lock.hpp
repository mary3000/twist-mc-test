#pragma once

#include <twist/logging/logging.hpp>
#include <twist/stdlike/atomic.hpp>
#include <twist/strand/spin_wait.hpp>
#include <twist/test_framework/test_framework.hpp>

#include <twist/checker/core/trace/trace.hpp>

using twist::checker::StateDescription;

class TicketLock {
 public:
  explicit TicketLock(size_t limit = std::numeric_limits<size_t>::max())
      : limit_{limit} {
  }

  void Lock() __attribute__((noinline)) {
    size_t this_thread_ticket{0};
    this_thread_ticket = next_free_ticket_.fetch_add(1);

    // twist::SpinWait spin_wait;
    while (this_thread_ticket != owner_ticket_.load()) {
      // spin_wait();
    }
  }

  bool TryLock() {
    size_t expected = owner_ticket_.load();
    return next_free_ticket_.compare_exchange_strong(expected, expected + 1);
  }

  // Don't change this method
  void Unlock() {
    size_t owner = owner_ticket_.load();
    owner_ticket_.store(owner + 1);
  }

  StateDescription DescribeState() {
    StateDescription d{"TicketLock"};
    d.Add("owner_ticket_", owner_ticket_.load());
    d.Add("next_free_ticket_", next_free_ticket_.load());
    return d;
  }

 private:
  const size_t limit_;

  twist::stdlike::bounded_atomic<size_t> next_free_ticket_{0, limit_};
  twist::stdlike::bounded_atomic<size_t> owner_ticket_{0, limit_};
};
