#include <iostream>

#include <twist/test_framework/test_framework.hpp>

#include <twist/checker/core/api.hpp>

#include <twist/stdlike/thread.hpp>

#include <cmath>
#include <string>
#include <twist/checker/test.hpp>

#include "ticket_lock.hpp"

using namespace std::literals::chrono_literals;

using namespace twist::checker;

void CriticalSection(bool& in_critical_section) {
  std::string report = "Mutual exclusion violated";

  CHECKER_ASSERT(!in_critical_section, report);
  in_critical_section = true;

  CHECKER_ASSERT(in_critical_section, report);
  in_critical_section = false;
}

TEST_SUITE(TicketLock) {
  SIMPLE_CHECKER_TEST(EndlessLoop) {
    const static size_t kLimit = 3;
    const static size_t kThreads = 3;

    TicketLock lock{kLimit};
    bool in_critical_section = false;

    Trace("/tmp/ticket_lock/endless_loop.log");

    PrintState([&lock, &in_critical_section]() {
      StateDescription d{"LockState"};
      d.Add(lock.DescribeState());
      d.Add("in_critical_section_", in_critical_section);
      return d;
    });

    auto routine = [&]() {
      while (true) {
        lock.Lock();
        CriticalSection(in_critical_section);
        lock.Unlock();
      }
    };

    std::vector<twist::stdlike::thread> threads;
    for (size_t i = 0; i < kThreads; ++i) {
      threads.emplace_back(routine);
    }

    for (auto& th : threads) {
      th.join();
    }
  }

  SIMPLE_CHECKER_TEST(AtomicOverflow) {
    const static size_t kLimit = 3;
    const static size_t kThreads = 2;

    TicketLock lock{kLimit};
    bool in_critical_section = false;

    FailsExpected();
    Trace("/tmp/ticket_lock/atomic_overflow.log");

    PrintState([&lock, &in_critical_section]() {
      StateDescription d{"LockState"};
      d.Add(lock.DescribeState());
      d.Add("in_critical_section_", in_critical_section);
      return d;
    });

    auto routine = [&]() {
      while (true) {
        if (lock.TryLock()) {
          CriticalSection(in_critical_section);
          lock.Unlock();
        }
      }
    };

    std::vector<twist::stdlike::thread> threads;
    for (size_t i = 0; i < kThreads; ++i) {
      threads.emplace_back(routine);
    }

    for (auto& th : threads) {
      th.join();
    }
  }
}

RUN_ALL_TESTS()
