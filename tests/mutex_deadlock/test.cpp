#include <twist/test_framework/test_framework.hpp>

#include <twist/checker/core/api.hpp>
#include <twist/checker/test.hpp>

#include <twist/stdlike/thread.hpp>
#include <twist/stdlike/mutex.hpp>

#include <cmath>
#include <string>
#include <twist/stdlike/condition_variable.hpp>

using namespace std::literals::chrono_literals;

using namespace twist::checker;

TEST_SUITE(MutexDeadlock) {
  SIMPLE_CHECKER_TEST(FinnJake) {
    ExpectDeadlock();

    twist::stdlike::mutex finn_mutex;
    twist::stdlike::mutex jake_mutex;

    Trace("/tmp/checker/mutex_deadlock.log");

    ShowLocals(false);

    twist::stdlike::thread finn([&]() {
      finn_mutex.lock();
      jake_mutex.lock();

      jake_mutex.unlock();
      finn_mutex.unlock();
    });

    twist::stdlike::thread jake([&]() {
      jake_mutex.lock();
      finn_mutex.lock();

      finn_mutex.unlock();
      jake_mutex.unlock();
    });

    finn.join();
    jake.join();
  }
}

RUN_ALL_TESTS()
