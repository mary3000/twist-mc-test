#include <twist/test_framework/test_framework.hpp>

#include <twist/stdlike/mutex.hpp>
#include <twist/stdlike/thread.hpp>

#include <twist/checker/test.hpp>

#include <cmath>
#include <string>

#include "channel.hpp"

using namespace twist::checker;

TEST_SUITE(Channel) {
  SIMPLE_CHECKER_TEST(LostWakeup) {
    Trace("/tmp/checker/channel.log");
    ExpectDeadlock();

    static const size_t kItems = 2;
    Channel<int> items;

    auto consume = [&]() { items.Take(); };
    std::vector<twist::stdlike::thread> consumers;
    for (size_t i = 0; i < kItems; ++i) {
      consumers.emplace_back(consume);
    }

    twist::stdlike::thread producer([&]() {
      for (size_t i = 0; i < kItems; ++i) {
        items.Put(42);
      }
    });

    producer.join();
    for (auto& consumer : consumers) {
      consumer.join();
    }
  }
}

RUN_ALL_TESTS()
