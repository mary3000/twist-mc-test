#pragma once

#include <twist/stdlike/condition_variable.hpp>
#include <twist/stdlike/mutex.hpp>

#include <optional>
#include <deque>

template <typename T>
class Channel {
 public:
  // Returns false iff queue is closed / shutted down
  bool Put(T item) {
    std::lock_guard guard(mutex_);

    if (closed_) {
      return false;
    }

    items_.push_back(std::move(item));

    SHOW_NOTE("Item was put to channel");

    if (items_.size() == 1) {
      has_items_.notify_one();
    }
    return true;
  }

  // Await and dequeue next item
  // Returns false iff queue is both 1) drained and 2) closed
  std::optional<T> Take() {
    std::unique_lock lock(mutex_);

    while (items_.empty() && !closed_) {
      has_items_.wait(lock);
    }

    if (!items_.empty()) {
      T item = std::move(items_.front());
      items_.pop_front();

      SHOW_NOTE("Item was taken from channel");

      return std::move(item);
    }

    return std::nullopt;
  }

  // Close queue for producers
  void Close() {
    std::lock_guard guard(mutex_);

    closed_ = true;
    has_items_.notify_all();
  }

  // Close queue for producers and consumers,
  // discard existing items
  void Shutdown() {
    std::lock_guard guard(mutex_);

    closed_ = true;
    items_.clear();
    has_items_.notify_all();
  }

 private:
  std::deque<T> items_;

  twist::stdlike::condition_variable has_items_;
  twist::stdlike::mutex mutex_;

  bool closed_{false};
};