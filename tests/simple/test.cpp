#include <twist/checker/core/checker.hpp>
#include <twist/checker/test.hpp>
#include <twist/checker/core/event.hpp>

#include <twist/test_framework/test_framework.hpp>

#include <twist/stdlike/thread.hpp>

#include <iostream>

using namespace twist::checker;

TEST_SUITE(ModelChecker) {
  size_t MultinomialCoefficient(const size_t n, size_t k) {
    size_t total = n * k;
    size_t result = 1;

    for (size_t i = 0; i < n; ++i) {
      for (size_t j = 1; j <= k; ++j) {
        result *= total--;
        result /= j;
      }
    }

    return result;
  }

  SIMPLE_TEST(Yield) {
    static const size_t kThreads = 2;
    static const size_t kSteps = 3;
    static const size_t kPathLength = kSteps * kThreads;
    static const size_t kExpectedCombinations =
        MultinomialCoefficient(kThreads, kSteps);

    size_t combinations = 0;
    std::string first;
    std::string last;

    auto spawner = [&]() {
      size_t current_length = 0;
      char buffer[kPathLength];

      Event event;

      auto step = [&](char ch) {
        buffer[current_length++] = ch;

        if (current_length == kPathLength) {
          if (first.empty()) {
            first = {buffer, kPathLength};
          }
          last = {buffer, kPathLength};

          ++combinations;
          event.Signal();
        }
      };

      auto fiber = [step](char id) {
        for (size_t i = 0; i < kSteps - 1; ++i) {
          step(id);
          Yield();
        }
        step(id);
      };

      std::vector<twist::stdlike::thread> threads;
      char id = 'A';
      for (size_t i = 0; i < kThreads; ++i) {
        threads.emplace_back([id, &fiber]() { fiber(id); });
        ++id;
      }

      event.Await();

      for (auto& th : threads) {
        th.join();
      }
    };

    RunChecker(spawner);

    ASSERT_EQ(combinations, kExpectedCombinations);
    ASSERT_EQ(first, "AAABBB");
    ASSERT_EQ(last, "BBBAAA");
  }

  SIMPLE_CHECKER_TEST(DieHard) {
    static const size_t kWant = 4;
    static const size_t kBigCapacity = 5;
    static const size_t kSmallCapacity = 3;

    size_t big = 0;
    size_t small = 0;

    AddInvariant([&]() -> std::pair<bool, std::string> {
      return {big != kWant, "Solved"};
    }).ExpectFail();

    auto loop = [&](const std::function<void()>& step) {
      std::vector<long long> a(500);
      while (true) {
        step();
        MakeState();
      }
    };

    auto fill_big = [&]() { big = kBigCapacity; };

    auto fill_small = [&]() { small = kSmallCapacity; };

    auto empty_big = [&]() { big = 0; };

    auto empty_small = [&]() { small = 0; };

    auto small_to_big = [&]() {
      size_t delta = std::min((kBigCapacity - big), small);
      big += delta;
      small -= delta;
    };

    auto big_to_small = [&]() {
      size_t delta = std::min((kSmallCapacity - small), big);
      small += delta;
      big -= delta;
    };

    std::vector<twist::stdlike::thread> threads;

    threads.emplace_back([&]() { loop(fill_big); });
    threads.emplace_back([&]() { loop(fill_small); });

    threads.emplace_back([&]() { loop(empty_big); });
    threads.emplace_back([&]() { loop(empty_small); });

    threads.emplace_back([&]() { loop(small_to_big); });
    threads.emplace_back([&]() { loop(big_to_small); });

    for (auto& th : threads) {
      th.join();
    }
  }
}

RUN_ALL_TESTS()
