#include <iostream>

#include <twist/test_framework/test_framework.hpp>

#include <twist/checker/core/api.hpp>

#include <twist/stdlike/atomic.hpp>
#include <twist/stdlike/thread.hpp>

#include <twist/stdlike/condition_variable.hpp>
#include <twist/stdlike/mutex.hpp>

#include <cmath>
#include <string>
#include <twist/checker/test.hpp>

using namespace std::literals::chrono_literals;

using namespace twist::checker;

class ZeroAwaiter {
 public:
  void Up() {
    tokens_.fetch_add(1);
  }

  void Down() {
    tokens_.fetch_sub(1);
    zero_tokens_.notify_one();
  }

  void Await() {
    std::unique_lock lock(mutex_);
    while (tokens_.load() != 0) {
      zero_tokens_.wait(lock);
    }
  }

 private:
  twist::stdlike::condition_variable zero_tokens_;
  twist::stdlike::mutex mutex_;
  twist::stdlike::atomic<size_t> tokens_{0};
};

TEST_SUITE(ZeroAwaiter) {
  SIMPLE_CHECKER_TEST(Simple) {
    ZeroAwaiter awaiter;

    Trace("/tmp/checker/zero_awaiter_deadlock.log");
    ExpectDeadlock();

    twist::stdlike::thread producer([&awaiter]() {
      awaiter.Up();
      awaiter.Down();
    });

    twist::stdlike::thread waiter([&awaiter]() { awaiter.Await(); });

    waiter.join();
    producer.join();
  }
}

RUN_ALL_TESTS()
