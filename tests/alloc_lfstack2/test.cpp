#include <twist/test_framework/test_framework.hpp>

#include <twist/stdlike/thread.hpp>

#include <twist/checker/test.hpp>

#include <iostream>
#include <cmath>

#include "alloc_lfstack.hpp"
#include "memory.hpp"

using namespace twist::checker;

TEST_SUITE(AllocLockFreeStack) {
  SIMPLE_CHECKER_TEST(Spec) {
    MemoryPool pool{3};
    LFAllocator stack{pool};

    Trace("/tmp/alloc_lfstack/v2.log");

    PrintState([&stack, &pool]() {
      StateDescription d{"LFAlloc"};
      d.Add(stack.DescribeState());
      d.Add(pool.DescribeState());
      return d;
    });

    // optional
    AddInvariant([&stack]() -> std::pair<bool, std::string> {
      auto check_list = [](Node* node) {
        int i = 0;
        while (node != nullptr) {
          if (node->IsReleased()) {
            return false;
          }
          node = node->next_.load();
          ++i;
        }
        return true;
      };

      if (!check_list(stack.Top())) {
        return {false, "ItemList: assertion failed"};
      }

      if (!check_list(stack.TopPending())) {
        return {false, "PendingList: assertion failed"};
      }

      return {true, ""};
    }).ExpectFail();

    auto worker = [&stack]() {
      while (true) {
        Node* node{nullptr};

        if (stack.Alloc(node)) {
          stack.Free(node);
        }
      }
    };

    std::vector<twist::stdlike::thread> threads;
    const static size_t kThreads = 4;
    for (size_t i = 0; i < kThreads; ++i) {
      threads.emplace_back(worker);
    }

    for (auto& th : threads) {
      th.join();
    }
  }
}

RUN_ALL_TESTS()
