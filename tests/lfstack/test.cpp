#include <iostream>

#include <twist/test_framework/test_framework.hpp>

#include <twist/checker/test.hpp>

#include <twist/stdlike/thread.hpp>

#include <cmath>

#include "buggy_lfstack.hpp"

using namespace twist::checker;

TEST_SUITE(BuggyLFStack) {
  SIMPLE_CHECKER_TEST(PushPop) {
    static const int kExpected = 5;
    static const size_t kThreads = 3;

    FailsExpected();

    BuggyLockFreeStack<int> stack;

    FiberRoutine worker = [&]() {
      while (true) {
        stack.Push(kExpected);

        int result;
        bool popped = stack.Pop(result);

        CHECKER_ASSERT(popped, "empty stack");
        CHECKER_ASSERT(result == kExpected, "unexpected popped value");
      }
    };

    std::vector<twist::stdlike::thread> threads;
    for (size_t i = 0; i < kThreads; ++i) {
      threads.emplace_back(worker);
    }

    for (auto& th : threads) {
      th.join();
    }
  }
}

RUN_ALL_TESTS()
