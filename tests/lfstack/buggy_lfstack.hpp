#pragma once

#include <twist/stdlike/atomic.hpp>

template <typename T>
class BuggyLockFreeStack {
  struct Node {
    T item_;
    Node* next_{0};

    explicit Node(T item) : item_(std::move(item)) {
    }
  };

 public:
  ~BuggyLockFreeStack() {
    Node* cur = reinterpret_cast<Node*>(top_.load());

    while (cur != nullptr) {
      Node* tmp = cur->next_;
      delete cur;
      cur = tmp;
    }
  }

  void Push(T item) {
    Node* node = new Node(std::move(item));
    Node* expected_top = top_.load();

    do {
      node->next_ = expected_top;
    } while (!top_.compare_exchange_weak(expected_top, node));
  }

  bool Pop(T& item) {
    Node* top = top_.load();

    do {
      if (top == 0) {
        return false;
      }
    } while (!top_.compare_exchange_weak(top, top->next_));

    item = std::move(top->item_);

    delete top;

    return true;
  }

 private:
  twist::stdlike::atomic<Node*> top_{0};
};

