#include <twist/test_framework/test_framework.hpp>

#include <twist/stdlike/mutex.hpp>
#include <twist/stdlike/thread.hpp>

#include <twist/checker/test.hpp>

#include <cmath>
#include <string>

#include "dining.hpp"
#include "philosopher.hpp"

using namespace std::literals::chrono_literals;

using namespace twist::checker;

TEST_SUITE(Dining) {
  SIMPLE_CHECKER_TEST(Infinite) {
    ExpectDeadlock();
    Trace("/tmp/checker/dining.log");

    static const size_t kSeats = 5;

    dining::Table table{kSeats};

    std::vector<twist::stdlike::thread> workers;
    for (size_t i = 0; i < kSeats; ++i) {
      workers.emplace_back([&, i]() {
        dining::Philosopher phi{table, i};
        while (true) {
          phi.EatOneMoreTime();
        }
      });
    }

    for (auto& w : workers) {
      w.join();
    }
  }
}

RUN_ALL_TESTS()
