#include <twist/test_framework/test_framework.hpp>

#include <twist/support/random.hpp>

#include <twist/checker/test.hpp>
#include <twist/checker/core/checker.hpp>

#include <twist/stdlike/thread.hpp>

#include <iostream>
#include <cmath>

using namespace twist::checker;

size_t Inc(size_t i) {
  return (i + 1) % 5;
}

void DoSmth(size_t& counter, int& i, bool recurse = true) {
  int a = counter;
  int b = counter;

  counter = Inc(counter);
  MakeState();
  counter = Inc(counter);

  if (recurse) {
    DoSmth(counter, i, false);
  }

  if (i == 12345) {
    std::cout << a << b;
  }
}

static size_t kThreads = 3;

TEST_SUITE(InfiniteLoop) {
  SIMPLE_CHECKER_TEST(Loop) {
    size_t counter = 0;

    auto worker = [&counter]() {
      while (true) {
        int i = 0;
        DoSmth(counter, i);
      }
    };

    std::vector<twist::stdlike::thread> threads;
    for (size_t i = 0; i < kThreads; ++i) {
      threads.emplace_back(worker);
    }

    for (auto& th : threads) {
      th.join();
    }
  }

  SIMPLE_CHECKER_TEST(Restart) {
    size_t counter = 0;

    auto worker = [&counter]() {
      int i = 0;
      DoSmth(counter, i);
      GetCurrentModelChecker()->RestartFiber();
    };

    std::vector<twist::stdlike::thread> threads;
    for (size_t i = 0; i < kThreads; ++i) {
      threads.emplace_back(worker);
    }

    for (auto& th : threads) {
      th.join();
    }
  }
}

RUN_ALL_TESTS()
