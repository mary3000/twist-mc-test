#include <twist/fiber/sync/event.hpp>

#include <twist/test_framework/test_framework.hpp>

#include <twist/support/random.hpp>

#include <twist/stdlike/thread.hpp>

#include <twist/checker/test.hpp>

#include <iostream>
#include <cmath>

#include "alloc_lfstack.hpp"

using namespace twist::checker;

using Item = uint64_t;

static const int kValue = 42;

TEST_SUITE(AllocLockFreeStack) {
  SIMPLE_CHECKER_TEST(Rand) {
    static const size_t kThreads = 3;

    AllocLockFreeStack<Item> stack;

    AddInvariant([&stack]() -> std::pair<bool, std::string> {
      Item removed{0};
      bool success = stack.Top(removed);
      return {!success || removed == kValue, "Check: top != 42"};
    }).ExpectFail();

    Prune([&stack]() {
      return stack.ItemListSize() > 2 || stack.PendingListSize() > 2;
    });

    // Find([&stack]() { return stack.PendingListSize() != 0; });

    FiberRoutine worker = [&stack]() {
      Item removed = kValue;

      if (twist::RandomUInteger(1) == 1) {
        stack.Push(kValue);
      } else {
        stack.Pop(removed);
      }

      GetCurrentModelChecker()->RestartFiber();  // ~ while true
    };

    // Uncommented ~ 30 sec
    // Commented ~ too long
    {
      ForkGuard guard;
      stack.Push(kValue);
      stack.Push(kValue);
      stack.PushPending(kValue);
    }

    std::vector<twist::stdlike::thread> threads;
    for (size_t i = 0; i < kThreads; ++i) {
      threads.emplace_back(worker);
    }

    for (auto& th : threads) {
      th.join();
    }
  }
}
RUN_ALL_TESTS()
