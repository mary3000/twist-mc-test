#include <twist/stdlike/atomic.hpp>

struct BlockNode {
  twist::stdlike::atomic<BlockNode*> next_;
  virtual ~BlockNode() = default;
};

//////////////////////////////////////////////////////////////////

class LockFreeStack {
 public:
  ~LockFreeStack() {
    twist::checker::ForkGuard guard;

    BlockNode* current = top_.load();

    while (current != nullptr) {
      BlockNode* next = current->next_.load();
      delete current;
      current = next;
    }
  }

 public:
  void Push(BlockNode* node) {
    BlockNode* current_top = top_.load();
    do {
      node->next_.store(current_top);
    } while (!top_.compare_exchange_weak(current_top, node));
  }

  void Append(BlockNode* head) {
    if (head == nullptr) {
      return;
    }

    BlockNode* tail = head;
    {
      // Only current thread owns these nodes
      twist::checker::ForkGuard guard;

      BlockNode* next = tail->next_.load();
      while (next != nullptr) {
        tail = next;
        next = tail->next_.load();
      }
    }

    BlockNode* prev_top = top_.load();
    do {
      tail->next_ = prev_top;
    } while (!top_.compare_exchange_weak(prev_top, head));
  }

  BlockNode* Top() {
    return top_.load();
  }

  BlockNode* Pop() {
    BlockNode* curr_top = top_.load();
    do {
      if (curr_top == nullptr) {
        break;
      }
    } while (!top_.compare_exchange_weak(curr_top, curr_top->next_.load()));
    return curr_top;
  }

  bool TryClear(BlockNode* expected) {
    BlockNode* desired = nullptr;
    return top_.compare_exchange_strong(expected, desired);
  }

  size_t Size() const {
    twist::checker::ForkGuard guard;
    size_t size = 0;
    BlockNode* current = top_.load();
    while (current != nullptr) {
      ++size;
      current = current->next_.load();
    }
    return size;
  }

 private:
  twist::stdlike::atomic<BlockNode*> top_{nullptr};
};

//////////////////////////////////////////////////////////////////

template <typename T>
class AllocLockFreeStack {
 public:
  struct Node : BlockNode {
    T item_;

    explicit Node(T item) : item_(std::move(item)) {
    }
  };

 public:
  void Push(T item) __attribute__((noinline))
  /*__attribute__((force_align_arg_pointer))*/ {
    auto* node = new Node(std::move(item));

    if (allocation_count_.load() == 0) {
      item_list_.Push(node);
    } else {
      pending_list_.Push(node);
    }
  }

  bool Pop(T& item) __attribute__((noinline))
  __attribute__((force_align_arg_pointer)) {
    // auto keep_count = pending_count_.load();
    BlockNode* pending_head = pending_list_.Top();

    if (allocation_count_.fetch_add(1) == 0) {
      if (pending_head != nullptr /*&& keep_count == pending_count_.load()*/
          && pending_list_.TryClear(pending_head)) {
        BlockNode* head_block = pending_head;
        pending_head = pending_head->next_.load();

        item_list_.Append(pending_head);
        // pending_count_.fetch_add(1);
        allocation_count_.fetch_sub(1);

        twist::checker::MakeState();

        item = dynamic_cast<Node*>(head_block)->item_;
        delete head_block;
        return true;
      }
    }

    Node* block = dynamic_cast<Node*>(item_list_.Pop());
    allocation_count_.fetch_sub(1);
    if (block == nullptr) {
      // twist::fiber::GetCurrentModelChecker()->Prune();
      return false;
    }
    item = std::move(block->item_);
    delete block;
    return true;
  }

  void PushPending(T item) {
    auto* node = new Node(std::move(item));
    pending_list_.Push(node);
  }

  bool Top(T& item) {
    twist::checker::ForkGuard guard;
    auto* top = item_list_.Top();
    if (top == nullptr) {
      return false;
    }
    item = reinterpret_cast<Node*>(top)->item_;
    return true;
  }

  size_t ItemListSize() const {
    return item_list_.Size();
  }

  size_t PendingListSize() const {
    return pending_list_.Size();
  }

 private:
  LockFreeStack item_list_;
  LockFreeStack pending_list_;

  twist::stdlike::atomic<int> allocation_count_{0};
  // twist::stdlike::atomic<int> pending_count_{0};
};

/////////////////////////////////////////////////////////////////////
