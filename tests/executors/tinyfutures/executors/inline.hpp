#pragma once

#include <tinyfutures/executors/executor.hpp>

namespace executors {

// Executes scheduled tasks immediately on the current thread
IExecutorPtr GetInlineExecutor();

};  // namespace executors
