#include <tinyfutures/executors/static_thread_pool.hpp>

#include <tinyfutures/executors/queues.hpp>
#include <tinyfutures/executors/helpers.hpp>

#include <twist/stdlike/thread.hpp>
#include <twist/stdlike/atomic.hpp>
#include <twist/stdlike/condition_variable.hpp>
#include <iostream>

namespace executors {

using twist::checker::ForkGuarded;

class SimpleThreadPool final : public IThreadPool {
 public:
  SimpleThreadPool(size_t threads) {
    for (size_t i = 0; i < threads; ++i) {
      workers_.emplace_back([this]() { WorkerRoutine(); });
    }
  }

  ~SimpleThreadPool() {
    Shutdown();
  }

  // IExecutor

  void Execute(Task&& task) override {
    WorkCreated();
    tasks_->Put(std::move(task));
    SHOW_NOTE("Thread pool: task was put to queue");
  }

  void WorkCreated() override {
    work_count_.fetch_add(1);
  }

  void WorkCompleted() override {
    if (work_count_.fetch_sub(1) == 1) {
      if (joining_.load()) {
        tasks_->Close();
      }
    }
  }

  // IThreadPool

  void Join() override {
    joining_.store(true);
    if (work_count_.load() == 0) {
      tasks_->Close();
    }
    JoinWorkers();
  }

  void Shutdown() override {
    tasks_->Shutdown();
    JoinWorkers();
  }

  size_t ExecutedTaskCount() const override {
    return executed_count_.load();
  }

 private:
  void WorkerRoutine() {
    // LabelThread(name_);
    while (auto task = tasks_->Take()) {
      SHOW_NOTE("Thread pool: task was taken from queue");
      SafelyExecuteHere(task.value());
      SHOW_NOTE("Thread pool: task executed");
      WorkCompleted();
      executed_count_.fetch_add(1);
    }
  }

  void JoinWorkers() {
    if (joined_.load()) {
      return;
    }
    for (auto& worker : workers_) {
      worker.join();
    }
    joined_.store(true);
  }

 private:
  ForkGuarded<MPMCBlockingQueue<Task>> tasks_;
  std::vector<twist::stdlike::thread> workers_;
  twist::stdlike::atomic<size_t> work_count_{0};

  twist::stdlike::atomic<size_t> executed_count_{0};

  twist::stdlike::atomic<bool> joined_{false};
  twist::stdlike::atomic<bool> joining_{false};
};

IThreadPoolPtr MakeStaticThreadPool(size_t threads) {
  return std::make_shared<SimpleThreadPool>(threads);
}

}  // namespace executors
