#include <twist/test_framework/test_framework.hpp>

#include <twist/stdlike/mutex.hpp>
#include <twist/stdlike/thread.hpp>

#include <twist/checker/test.hpp>

#include <tinyfutures/executors/static_thread_pool.hpp>
#include <tinyfutures/executors/strand.hpp>

#include <cmath>
#include <string>

using namespace twist::checker;
using namespace executors;

TEST_SUITE(Executors) {
  SIMPLE_CHECKER_TEST(Strand) {
    Trace("/tmp/checker/strand.log");
    FailsExpected();
    ShowLocals(false);

    auto tp = MakeStaticThreadPool(1);
    auto strand = MakeStrand(tp);

    size_t tasks = 0;
    strand->Execute([&]() { ++tasks; });
    strand->Execute([&]() { ++tasks; });

    tp->Join();

    CHECKER_ASSERT(tasks == 2, "Expected 2 completed tasks, found " << tasks);
  }
}

RUN_ALL_TESTS()
