# Model checker for testing concurrent C++ programs


**Important:** this repo contains obsolete version of the checker. The tool has been separated from the testing framework ([twist](https://gitlab.com/Lipovsky/twist)), you can find the new version [here](https://gitlab.com/mary3000/fork).

---

The tool verifies a program by executing all possible thread interleavings. The approach is very similar to TLC, but the tested code should be written on C++ - it doesn't need to be translated to TLA+ or +Cal.  

Checker works under sequential consistency model and ignores relaxed atomic operations.

[Examples of tests](tests)

[Implementation](https://gitlab.com/mary3000/fork)

[Thesis (russian)](https://drive.google.com/file/d/1Y3H8Bp4naqeVxUoX_rFOSiXmlNHmC7t2/view?usp=sharing)

[Presentation (english)](https://docs.google.com/presentation/d/1Pz8ETY3246NhhqJ4OzSDWbZbUZN_xwxVn2K3hH9uaAc/edit?usp=sharing)
 / [Presentation (russian)](https://docs.google.com/presentation/d/1cRkSKH-prUZ9xb3w8ceHqrWHWLiPnVETZG8UyLBj8UE/edit?usp=sharing)

## Main ideas

Checker replaces all threads, synchronization primitives, atomics and allocations with its own implementation:
- Threads: they are replaced by fibers. It allows to make execution determenistic and to manually save / restore fiber stacks. 
- Primitives and atomics: new state is added into the state queue each time test accesses them.  
- Allocations: checker uses its own simple bump-pointer allocator, so that it is easy to make a snapshot from it.

## Acknowledgments 

[Roman Lipovsky](https://gitlab.com/Lipovsky). This work was done as a part of my Bachelor thesis, and it was not possible without my advisor. He wrote the testing framework [twist](https://gitlab.com/Lipovsky/twist) on top of which I initially implemented my model checker, designed the core of my model checker, gave me ideas, inspiration and taught me how to write.

## Prerequisites

Was only tested on:

_Architecture_: x86-64.

_OS_: Ubuntu.

_Compiler_: clang-8.

_Dependencies_: binutils.

## Getting started

1. Change namespace in your data structure / algorithm from `std` to `twist::stdlike` for the following objects: atomic, thread, mutex, condition_variable.
2. Write test (see [examples](tests)).
3. Execute test in _Explore_ mode first, and in _Trace_ mode if neccessary (see below).

## Modes

### Explore

Flags: `-DTWIST_CHECKER=1`

Finds all reachable states of a test. 

Result description - the same as in [TLC](https://tla.msr-inria.inria.fr/tlatoolbox/doc/model/results-page.html):

- __Diameter__  
    The diameter of the reachable-state graph.  It is the length of the longest behavior found so far.
- __States Found__  
    The total number of states checker has examined so far.
- __Distinct States__  
    The number of distinct states among the states found.
- __Queue Size__  
    The number of (distinct) states found whose successor states checker has not yet determined.

### Trace

Flags: `-DTWIST_CHECKER=1 -DTWIST_TRACE=1`

If bug found in the _Explore_ mode, re-executes buggy path and prints detailed trace. 

Trace describes thread switching history. History is divided into _runs_, which are contigious chunks of a particular thread execution. Run has one or more thread _steps_. 

Step consists of: 
 - __Shared state__. State representation should be provided by user via `PrintState` function.
 - __Stacktrace__ (thanks [backward-cpp](https://github.com/bombela/backward-cpp)) and __local variables__ (thanks gdb).  
 - __Notes__. Can be internal or provided by user via `SHOW_NOTE` macro.

## API

### What to check for

`CHECKER_ASSERT(condition, message)` - macro for local assertion. 

`Invariant& AddInvariant(MessagePredicate predicate)` - global invariant. Should be added in the first thread before spawning other threads.

### What to expect

`void ExpectDeadlock()` - expect that there exist a deadlock in the test. Should be added in the first thread before spawning other threads.

`void FailsExpected()` - expect any fail. Should be added in the first thread before spawning other threads.

### State control

`void Prune(Predicate predicate)` - prune state on some predicate. Should be added in the first thread before spawning other threads.

`void MakeState()` - make new state from the current place.

`size_t Random(size_t max)` - return "random" value from `0` to `max-1`. Creates `max` states. 

`bool Either()` - Random(2).

### Trace mode

`void PrintState(Describer describer)` - describe how to print state in the trace mode. Should be added in the first thread before spawning other threads.

`void Trace(const std::string& path)` - path where trace to bug should be saved. Should be added in the first thread before spawning other threads.

`void ShowLocals(bool flag = true)` - set whether local variables should be presented in trace. Should be added in the first thread before spawning other threads.

`SHOW_NOTE(note)` - show note during trace re-execution.            

`void ShowLocalState()` - show local state.

`void ShowState()` - show global state.
